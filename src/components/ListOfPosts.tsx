import React from "react";
import { IPost } from "../interfaces";

type PostList = {
  postsForShowing: IPost[]
}

const ListOfPosts: React.FC<PostList> = ({postsForShowing}) => {
  return (
    <ul>
      {postsForShowing.map(post => (
        <li key={post.id}>{post.title}</li>
      ))}
    </ul>
  );
};

export default ListOfPosts;
