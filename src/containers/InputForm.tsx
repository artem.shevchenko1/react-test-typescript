import React from 'react'

interface IText {
  text: string
  setText(title: string): void
}

const InputForm: React.FC<IText> = (props) => {
  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setText(event.target.value);
  }

  return (
    <label>
      Введите текст для поиска:
      <input
        type="text"
        id="inputText"
        value={props.text}
        placeholder="Введите текст"
        onChange={changeHandler}
      />
    </label>
  )
}

export default InputForm;