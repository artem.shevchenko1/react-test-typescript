import React, {useState, useEffect} from 'react';
import './App.css';
import ListOfPosts from './components/ListOfPosts';
import { IPost } from './interfaces';
import InputForm from './containers/InputForm';

const App: React.FC = () => {
  const [downloadedPosts, setDownloadedPosts] = useState<IPost[]>([]);
  const [postsForShowing, setPostsForShowing] = useState<IPost[]>([]);
  const [text, setText] = useState<string>('');

  useEffect(() => {
    const fetchMyAPI = async () => {
      const responsePosts = await 
        fetch('https://jsonplaceholder.typicode.com/posts');
      const fetchedPosts = await responsePosts.json();
      console.log(fetchedPosts);
      setDownloadedPosts(fetchedPosts);
      setPostsForShowing(fetchedPosts);
    };
    
    fetchMyAPI();  
  }, []);

  useEffect(() => {
    setPostsForShowing(downloadedPosts
      .filter(post => post.title.includes(text)));
  }, [text, downloadedPosts]);

  return (
    <div className="App">
      <InputForm 
        text={text}
        setText={setText}
      />
      <ListOfPosts 
        postsForShowing={postsForShowing}
      />
    </div>
  );
}

export default App;
